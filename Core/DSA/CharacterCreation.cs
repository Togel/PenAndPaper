﻿namespace Core.DSA
{
    enum RaceEnum
    {
        Default,
        Mittellaender,
        Tulamiden,
        Thorwaler,
        Nivesen,
        Norbarden,
        Trollzacker,
        Waldmenschen,
        Utulus,
        Elfen,
        Halbelfen,
        Zwerge,
        Orks,
        Halborks,
        Goblins,
        Achaz
    }

    enum TrollzackerEnum
    {
        Default,
        Rochshaz
    }

    enum WaldmenschEnum
    {
        Default,
        Tocamuyac
    }

    enum CultureEnum
    {
        None,
        MittellaendischeStaedte,
        MittellaendischeLandbevoelkerung,
        AndergastUndNostria,
        Bornland,
        SvellttalUndNordlande,
        Almada,
        Horasreich,
        Zyklopeninseln,
        Amazonenburg,
        Aranien,
        Mhanadistan,
        TulamidischeStadtstaaten,
        Novadi,
        Ferkina,
        Zahori,
        Thorwal,
        Gjalskerland,
        Fjarninger,
        Dschungelstaemme,
        VerloreneStaemme,
        WaldinselUtutlus,
        Miniwatu,
        Tocamuyac,
        Maraskan,
        Suedaventurien,
        Bukanier,
        Nivesenstaemme,
        NuanaaeLie,
        Norbardensippe,
        Trollzacken,
        AuelfischeSippe,
        ElfischeSiedlung,
        SteppenelfischeSippe,
        WaldelfischeSippe,
        FirnelfischeSippe,
        Ambosszwerge,
        Erzzwerge,
        Huegelzwerge,
        Brillantzwerge,
        Brobim,
        Orkland,
        Yurach,
        SvellttalBesatzer,
        Goblinstamm,
        Goblinbande,
        FestumerGhetto,
        ArchaischeAchaz,
        StammesAchaz
    }

    enum ProfessionEnum
    {

    }

    class CharacterCreation
    {
        public System.Random rand = new System.Random();

        void CreateCharacter()
        {
            Character character = new Character();
            Name name = new Name();
            GenerationPoints gpBasis = new GenerationPoints();

            
        
            Sex sex = new Sex();
            //Age/Birthdate
            //Height height = new Height();
            //Weight weight = new Weight();
            //HairColour hairColour = new HairColour();
            //EyeColour eyeColour = new EyeColour();
            Appearance appearance = new Appearance();


            character.AddComponent(name);
            character.AddComponent(gpBasis);
            character.AddComponent(sex);





        }

        void CreateRace(Character character, RaceEnum eRace)
        {
            switch(eRace)
            {
                case RaceEnum.Mittellaender:
                    CreateMittellaender(character);
                    break;

                case RaceEnum.Tulamiden:
                    CreateTulamide(character);
                    break;

                case RaceEnum.Thorwaler:
                    CreateThorwaler(character);
                    break;

                case RaceEnum.Nivesen:
                    CreateNivese(character);
                    break;

                case RaceEnum.Norbarden:
                    CreateNorbarde(character);
                    break;

                case RaceEnum.Trollzacker:
                    CreateTrollzacker(character);
                    break;

                case RaceEnum.Waldmenschen:
                    CreateWaldmensch(character);
                    break;

                case RaceEnum.Utulus:
                    CreateUtulu(character);
                    break;

                case RaceEnum.Elfen:
                    CreateElf(character);
                    break;

                case RaceEnum.Halbelfen:
                    CreateHalbelf(character);
                    break;

                case RaceEnum.Zwerge:
                    CreateZwerg(character);
                    break;

                case RaceEnum.Orks:
                    CreateOrk(character);
                    break;

                case RaceEnum.Halborks:
                    CreateHalbork(character);
                    break;

                case RaceEnum.Goblins:
                    CreateGoblin(character);
                    break;

                case RaceEnum.Achaz:
                    CreateAchaz(character);
                    break;
            }


        }

        bool CreateMittellaender(Character character)
        {
            Race race = new Race();
            race.SetData("Mittelländer");
            if(!character.AddComponent(race))
            {
                return false;
            }

            GenerationPoints gpCost = new GenerationPoints();
            gpCost.SetData(0);
            if (!character.AddComponent(gpCost))
            {
                return false;
            }

            HairColour hairColour = new HairColour();
            int hairD20 = rand.Next(1, 21);
            switch(hairD20)
            {
                case 1:
                case 2:
                case 3:
                    hairColour.SetData("schwarz");
                    break;

                case 4:
                case 5:
                case 6:
                case 7:
                    hairColour.SetData("braun");
                    break;

                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                    hairColour.SetData("dunkelblond");
                    break;

                case 13:
                case 14:
                case 15:
                case 16:
                    hairColour.SetData("blond");
                    break;

                case 17:
                case 18:
                    hairColour.SetData("weißblond");
                    break;

                case 19:
                case 20:
                    hairColour.SetData("rot");
                    break;
            }
            if (!character.AddComponent(hairColour))
            {
                return false;
            }

            EyeColour eyeColour = new EyeColour();
            int eyeD20 = rand.Next(1, 21);
            switch (eyeD20)
            {
                case 1:
                case 2:
                    eyeColour.SetData("dunkelbraun");
                    break;
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    eyeColour.SetData("braun");
                    break;
                case 10:
                case 11:
                    eyeColour.SetData("grün");
                    break;
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                    eyeColour.SetData("blau");
                    break;
                case 18:
                case 19:
                    eyeColour.SetData("grau");
                    break;
                case 20:
                    eyeColour.SetData("schwarz");
                    break;
            }
            if (!character.AddComponent(eyeColour))
            {
                return false;
            }

            Height height = new Height();
            int i = 160 + rand.Next(1, 21) + rand.Next(1, 21);
            height.SetData(i);
            if (!character.AddComponent(height))
            {
                return false;
            }

            Weight weight = new Weight();
            weight.SetData(i - 100);
            if (!character.AddComponent(weight))
            {
                return false;
            }

            VitalityModificator LEMod = new VitalityModificator();
            LEMod.SetData(10);
            if(!character.AddComponent(LEMod))
            {
                return false;
            }

            EnduranceModificator AUMod = new EnduranceModificator();
            AUMod.SetData(10);
            if (!character.AddComponent(AUMod))
            {
                return false;
            }

            MagicalResistanceModificator MRMod = new MagicalResistanceModificator();
            MRMod.SetData(-4);
            if (!character.AddComponent(MRMod))
            {
                return false;
            }

            InappropriateAdvantages inappropriateAdvantages = new InappropriateAdvantages();
            Advantage advantage = new Advantage();
            advantage.SetData("Herausragende Balance", 0);
            inappropriateAdvantages.AddComponent(advantage);
            if(!character.AddComponent(inappropriateAdvantages))
            {
                return false;
            }

            InappropriateHandicaps inappropriateHandicaps = new InappropriateHandicaps();
            Handicap handicap = new Handicap();
            handicap.SetData("Nahrungsrestriktion", 0);
            inappropriateHandicaps.AddComponent(handicap);
            if (!character.AddComponent(inappropriateHandicaps))
            {
                return false;
            }

            CommonCultures commonCultures = new CommonCultures();
            Culture culture = new Culture();
            culture.SetData("Mittelländische Städte");
            commonCultures.AddComponent(culture);
            culture.SetData("Mittelländische Landbevölkerung");
            commonCultures.AddComponent(culture);
            culture.SetData("Andergast und Nostria");
            commonCultures.AddComponent(culture);
            culture.SetData("Bornland");
            commonCultures.AddComponent(culture);
            culture.SetData("Svellttal");
            commonCultures.AddComponent(culture);
            culture.SetData("Almada");
            commonCultures.AddComponent(culture);
            culture.SetData("Horasreich");
            commonCultures.AddComponent(culture);
            culture.SetData("Zyklopeninseln");
            commonCultures.AddComponent(culture);
            culture.SetData("Maraskan");
            commonCultures.AddComponent(culture);
            culture.SetData("Südaventurien");
            commonCultures.AddComponent(culture);
            culture.SetData("Bukanier");
            commonCultures.AddComponent(culture);
            if (!character.AddComponent(commonCultures))
            {
                return false;
            }

            PossibleCultures possibleCultures = new PossibleCultures();
            // Culture culture = new Culture();
            culture.SetData("Aranien");
            possibleCultures.AddComponent(culture);
            culture.SetData("Tulamidische Stadtstaaten");
            possibleCultures.AddComponent(culture);
            culture.SetData("Thorwal");
            possibleCultures.AddComponent(culture);
            culture.SetData("Mhanadistan");
            possibleCultures.AddComponent(culture);
            culture.SetData("Amazonenburg");
            possibleCultures.AddComponent(culture);
            if (!character.AddComponent(possibleCultures))
            {
                return false;
            }


            return true;
        }

        bool CreateTulamide(Character character)
        {
            Race race = new Race();
            race.SetData("Tulamide");
            if (!character.AddComponent(race))
            {
                return false;
            }

            GenerationPoints gpCost = new GenerationPoints();
            gpCost.SetData(0);
            if (!character.AddComponent(gpCost))
            {
                return false;
            }
            //HairColour hairColour = new HairColour();
            //EyeColour eyeColour = new EyeColour();
            //Height height = new Height();
            //Weight weight = new Weight();

            VitalityModificator LEMod = new VitalityModificator();
            LEMod.SetData(10);
            if (!character.AddComponent(LEMod))
            {
                return false;
            }

            EnduranceModificator AUMod = new EnduranceModificator();
            AUMod.SetData(10);
            if (!character.AddComponent(AUMod))
            {
                return false;
            }

            MagicalResistanceModificator MRMod = new MagicalResistanceModificator();
            MRMod.SetData(-4);
            if (!character.AddComponent(MRMod))
            {
                return false;
            }

            InappropriateAdvantages inappropriateAdvantages = new InappropriateAdvantages();
            Advantage advantage = new Advantage();
            advantage.SetData("Herausragende Balance", 0);
            inappropriateAdvantages.AddComponent(advantage);
            if (!character.AddComponent(inappropriateAdvantages))
            {
                return false;
            }

            InappropriateHandicaps inappropriateHandicaps = new InappropriateHandicaps();
            Handicap handicap = new Handicap();
            handicap.SetData("Nahrungsrestriktion", 0);
            inappropriateHandicaps.AddComponent(handicap);
            if (!character.AddComponent(inappropriateHandicaps))
            {
                return false;
            }

            CommonCultures commonCultures = new CommonCultures();
            Culture culture = new Culture();
            culture.SetData("Aranien");
            commonCultures.AddComponent(culture);
            culture.SetData("Mhanadistan");
            commonCultures.AddComponent(culture);
            culture.SetData("Tulamidische Stadtstaaten");
            commonCultures.AddComponent(culture);
            culture.SetData("Novadi");
            commonCultures.AddComponent(culture);
            culture.SetData("Ferkina");
            commonCultures.AddComponent(culture);
            culture.SetData("Zahori");
            commonCultures.AddComponent(culture);
            culture.SetData("Maraskan");
            commonCultures.AddComponent(culture);
            culture.SetData("Südaventurien");
            commonCultures.AddComponent(culture);
            culture.SetData("Bukanier");
            commonCultures.AddComponent(culture);
            if (!character.AddComponent(commonCultures))
            {
                return false;
            }

            PossibleCultures possibleCultures = new PossibleCultures();
            culture.SetData("Mittelländische Städte");
            possibleCultures.AddComponent(culture);
            culture.SetData("Almada");
            possibleCultures.AddComponent(culture);
            culture.SetData("Amazonenburg");
            possibleCultures.AddComponent(culture);
            if (!character.AddComponent(possibleCultures))
            {
                return false;
            }

            return true;
        }

        bool CreateThorwaler(Character character)
        {
            Race race = new Race();
            race.SetData("Thorwaler");
            if (!character.AddComponent(race))
            {
                return false;
            }

            GenerationPoints gpCost = new GenerationPoints();
            gpCost.SetData(5);
            if (!character.AddComponent(gpCost))
            {
                return false;
            }
            //HairColour hairColour = new HairColour();
            //EyeColour eyeColour = new EyeColour();
            //Height height = new Height();
            //Weight weight = new Weight();

            CourageModificator MuMod = new CourageModificator();
            MuMod.SetData(1);
            if (!character.AddComponent(MuMod))
            {
                return false;
            }

            ConstitutionModificator KOMod = new ConstitutionModificator();
            KOMod.SetData(1);
            if (!character.AddComponent(KOMod))
            {
                return false;
            }

            StrengthModificator KKMod = new StrengthModificator();
            KKMod.SetData(1);
            if (!character.AddComponent(KKMod))
            {
                return false;
            }

            VitalityModificator LEMod = new VitalityModificator();
            LEMod.SetData(11);
            if (!character.AddComponent(LEMod))
            {
                return false;
            }

            EnduranceModificator AUMod = new EnduranceModificator();
            AUMod.SetData(10);
            if (!character.AddComponent(AUMod))
            {
                return false;
            }

            MagicalResistanceModificator MRMod = new MagicalResistanceModificator();
            MRMod.SetData(-5);
            if (!character.AddComponent(MRMod))
            {
                return false;
            }

            Handicaps handicaps = new Handicaps();
            Handicap handicap = new Handicap();
            handicap.SetData("Jähzorn", 6);
            handicaps.AddComponent(handicap);
            if (!character.AddComponent(handicaps))
            {
                return false;
            }

            RecommendedAdvantages recommendedAdvantages = new RecommendedAdvantages();
            Advantage advantage = new Advantage();
            advantage.SetData("Eisern", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Hohe Lebenskraft", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Innerer Kompass", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Kälteresistenz", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Kampfrausch", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Richtungssinn", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Zäher Hund", 0);
            recommendedAdvantages.AddComponent(advantage);
            if (!character.AddComponent(recommendedAdvantages))
            {
                return false;
            }

            RecommendedHandicaps recommendedHandicaps = new RecommendedHandicaps();
            handicap.SetData("Blutrausch", 0);
            recommendedHandicaps.AddComponent(handicap);
            handicap.SetData("Hitzeempfindlich", 0);
            recommendedHandicaps.AddComponent(handicap);
            if (!character.AddComponent(recommendedHandicaps))
            {
                return false;
            }

            InappropriateAdvantages inappropriateAdvantages = new InappropriateAdvantages();
            advantage.SetData("Herausragende Balance", 0);
            inappropriateAdvantages.AddComponent(advantage);
            advantage.SetData("Hohe Magieresistenz", 0);
            inappropriateAdvantages.AddComponent(advantage);
            if (!character.AddComponent(inappropriateAdvantages))
            {
                return false;
            }

            InappropriateHandicaps inappropriateHandicaps = new InappropriateHandicaps();
            handicap.SetData("Kleinwüchsig", 0);
            inappropriateHandicaps.AddComponent(handicap);
            handicap.SetData("Nahrungsrestriktion", 0);
            inappropriateHandicaps.AddComponent(handicap);
            if (!character.AddComponent(inappropriateHandicaps))
            {
                return false;
            }

            CommonCultures commonCultures = new CommonCultures();
            Culture culture = new Culture();
            culture.SetData("Thorwal");
            commonCultures.AddComponent(culture);
            culture.SetData("Fjarninger");
            commonCultures.AddComponent(culture);
            culture.SetData("Gjalskerland");
            commonCultures.AddComponent(culture);
            if (!character.AddComponent(commonCultures))
            {
                return false;
            }

            PossibleCultures possibleCultures = new PossibleCultures();
            culture.SetData("Mittelländische Städte (Siedlerstädte des Nordens, Hafenstädte)");
            possibleCultures.AddComponent(culture);
            culture.SetData("Mittelländische Landbevölkerung");
            possibleCultures.AddComponent(culture);
            culture.SetData("Andergast und Nostria");
            possibleCultures.AddComponent(culture);
            culture.SetData("Bornland");
            possibleCultures.AddComponent(culture);
            culture.SetData("Svellttal");
            possibleCultures.AddComponent(culture);
            culture.SetData("Südaventurien");
            possibleCultures.AddComponent(culture);
            culture.SetData("Tulamidische Stadtstaaten");
            possibleCultures.AddComponent(culture);
            culture.SetData("Bukanier");
            possibleCultures.AddComponent(culture);
            if (!character.AddComponent(possibleCultures))
            {
                return false;
            }

            Skill athletics = new Skill();
            Name name = new Name();
            SkillPoints skillPoints = new SkillPoints();
            name.SetData("Athletik");
            skillPoints.SetData(1);
            athletics.AddComponent(name);
            athletics.AddComponent(skillPoints);
            if (!character.AddComponent(athletics))
            {
                return false;
            }

            Skill sharpSenses = new Skill();
            name.SetData("Sinnesschärfe");
            skillPoints.SetData(1);
            sharpSenses.AddComponent(name);
            sharpSenses.AddComponent(skillPoints);
            if (!character.AddComponent(sharpSenses))
            {
                return false;
            }

            Skill carouse = new Skill();
            name.SetData("Zechen");
            skillPoints.SetData(1);
            carouse.AddComponent(name);
            carouse.AddComponent(skillPoints);
            if (!character.AddComponent(carouse))
            {
                return false;
            }

            Skill weatherForecast = new Skill();
            name.SetData("Wettervorhersage");
            skillPoints.SetData(1);
            weatherForecast.AddComponent(name);
            weatherForecast.AddComponent(skillPoints);
            if (!character.AddComponent(weatherForecast))
            {
                return false;
            }
            
            return true;
        }

        bool CreateNivese(Character character)
        {
            Race race = new Race();
            race.SetData("Nivese");
            if (!character.AddComponent(race))
            {
                return false;
            }

            GenerationPoints gpCost = new GenerationPoints();
            gpCost.SetData(4);
            if (!character.AddComponent(gpCost))
            {
                return false;
            }
            //HairColour hairColour = new HairColour();
            //EyeColour eyeColour = new EyeColour();
            //Height height = new Height();
            //Weight weight = new Weight();

            IntuitionModificator INMod = new IntuitionModificator();
            INMod.SetData(1);
            if (!character.AddComponent(INMod))
            {
                return false;
            }

            ConstitutionModificator KO = new ConstitutionModificator();
            KO.SetData(1);
            if (!character.AddComponent(KO))
            {
                return false;
            }
            
            VitalityModificator LEMod = new VitalityModificator();
            LEMod.SetData(9);
            if (!character.AddComponent(LEMod))
            {
                return false;
            }

            EnduranceModificator AUMod = new EnduranceModificator();
            AUMod.SetData(12);
            if (!character.AddComponent(AUMod))
            {
                return false;
            }

            MagicalResistanceModificator MRMod = new MagicalResistanceModificator();
            MRMod.SetData(-5);
            if (!character.AddComponent(MRMod))
            {
                return false;
            }

            RecommendedAdvantages recommendedAdvantages = new RecommendedAdvantages();
            Advantage advantage = new Advantage();
            advantage.SetData("Ausdauernd", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Entfernungssinn", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Gefahreninstinkt", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Innerer Kompass", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Kälteresistenz", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Richtungssinn", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Wolfskind", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Zäher Hund", 0);
            recommendedAdvantages.AddComponent(advantage);
            if (!character.AddComponent(recommendedAdvantages))
            {
                return false;
            }

            RecommendedHandicaps recommendedHandicaps = new RecommendedHandicaps();
            Handicap handicap = new Handicap();
            handicap.SetData("Hitzeempfindlich", 0);
            recommendedHandicaps.AddComponent(handicap);
            if (!character.AddComponent(recommendedHandicaps))
            {
                return false;
            }

            InappropriateAdvantages inappropriateAdvantages = new InappropriateAdvantages();
            advantage.SetData("Herausragende Balance", 0);
            inappropriateAdvantages.AddComponent(advantage);
            advantage.SetData("Hitzeresistenz", 0);
            inappropriateAdvantages.AddComponent(advantage);
            if (!character.AddComponent(inappropriateAdvantages))
            {
                return false;
            }

            InappropriateHandicaps inappropriateHandicaps = new InappropriateHandicaps();
            handicap.SetData("Kälteempfindlich", 0);
            inappropriateHandicaps.AddComponent(handicap);
            handicap.SetData("Nahrungsrestriktion", 0);
            inappropriateHandicaps.AddComponent(handicap);
            handicap.SetData("Platzangst", 0);
            inappropriateHandicaps.AddComponent(handicap);
            if (!character.AddComponent(inappropriateHandicaps))
            {
                return false;
            }

            CommonCultures commonCultures = new CommonCultures();
            Culture culture = new Culture();
            culture.SetData("Nivesenstämme");
            commonCultures.AddComponent(culture);
            culture.SetData("Nuanaä-Lie");
            commonCultures.AddComponent(culture);
            if (!character.AddComponent(commonCultures))
            {
                return false;
            }

            PossibleCultures possibleCultures = new PossibleCultures();
            culture.SetData("Mittelländische Städte (Siedlerstädte des Nordens)");
            possibleCultures.AddComponent(culture);
            culture.SetData("Mittelländische Landbevölkerung");
            possibleCultures.AddComponent(culture);
            culture.SetData("Bornland");
            possibleCultures.AddComponent(culture);
            culture.SetData("Svellttal");
            possibleCultures.AddComponent(culture);
            culture.SetData("Thorwal");
            possibleCultures.AddComponent(culture);
            culture.SetData("Gjalskerland");
            possibleCultures.AddComponent(culture);
            culture.SetData("Fjarninger");
            possibleCultures.AddComponent(culture);
            culture.SetData("Norbardensippen");
            possibleCultures.AddComponent(culture);
            if (!character.AddComponent(possibleCultures))
            {
                return false;
            }

            Skill sneaking = new Skill();
            Name name = new Name();
            SkillPoints skillPoints = new SkillPoints();
            name.SetData("Schleichen");
            skillPoints.SetData(1);
            sneaking.AddComponent(name);
            sneaking.AddComponent(skillPoints);
            if (!character.AddComponent(sneaking))
            {
                return false;
            }

            Skill selfControl = new Skill();
            name.SetData("Sinnesschärfe");
            skillPoints.SetData(1);
            selfControl.AddComponent(name);
            selfControl.AddComponent(skillPoints);
            if (!character.AddComponent(selfControl))
            {
                return false;
            }

            Skill orientation = new Skill();
            name.SetData("Zechen");
            skillPoints.SetData(1);
            orientation.AddComponent(name);
            orientation.AddComponent(skillPoints);
            if (!character.AddComponent(orientation))
            {
                return false;
            }

            return true;
        }

        bool CreateNorbarde(Character character)
        {
            Race race = new Race();
            race.SetData("Norbarde");
            if (!character.AddComponent(race))
            {
                return false;
            }

            GenerationPoints gpCost = new GenerationPoints();
            gpCost.SetData(3);
            if (!character.AddComponent(gpCost))
            {
                return false;
            }
            //HairColour hairColour = new HairColour();
            //EyeColour eyeColour = new EyeColour();
            //Height height = new Height();
            //Weight weight = new Weight();

            CharismaModificator CHMod = new CharismaModificator();
            CHMod.SetData(1);
            if (!character.AddComponent(CHMod))
            {
                return false;
            }

            VitalityModificator LEMod = new VitalityModificator();
            LEMod.SetData(11);
            if (!character.AddComponent(LEMod))
            {
                return false;
            }

            EnduranceModificator AUMod = new EnduranceModificator();
            AUMod.SetData(10);
            if (!character.AddComponent(AUMod))
            {
                return false;
            }

            MagicalResistanceModificator MRMod = new MagicalResistanceModificator();
            MRMod.SetData(-4);
            if (!character.AddComponent(MRMod))
            {
                return false;
            }

            RecommendedAdvantages recommendedAdvantages = new RecommendedAdvantages();
            Advantage advantage = new Advantage();
            advantage.SetData("Gefahreninstinkt", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Gutes Gedächtnis", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Innerer Kompass", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Kälteresistenz", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Richtungssinn", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Zäher Hund", 0);
            recommendedAdvantages.AddComponent(advantage);
            if (!character.AddComponent(recommendedAdvantages))
            {
                return false;
            }

            InappropriateAdvantages inappropriateAdvantages = new InappropriateAdvantages();
            advantage.SetData("Herausragende Balance", 0);
            inappropriateAdvantages.AddComponent(advantage);
            advantage.SetData("Hitzeresistenz", 0);
            inappropriateAdvantages.AddComponent(advantage);
            if (!character.AddComponent(inappropriateAdvantages))
            {
                return false;
            }

            InappropriateHandicaps inappropriateHandicaps = new InappropriateHandicaps();
            Handicap handicap = new Handicap();
            handicap.SetData("Nahrungsrestriktion", 0);
            inappropriateHandicaps.AddComponent(handicap);
            handicap.SetData("Platzangst", 0);
            inappropriateHandicaps.AddComponent(handicap);
            if (!character.AddComponent(inappropriateHandicaps))
            {
                return false;
            }

            CommonCultures commonCultures = new CommonCultures();
            Culture culture = new Culture();
            culture.SetData("Norbardensippe");
            commonCultures.AddComponent(culture);
            if (!character.AddComponent(commonCultures))
            {
                return false;
            }

            PossibleCultures possibleCultures = new PossibleCultures();
            culture.SetData("Mittelländische Städte (Siedlerstädte des Nordens)");
            possibleCultures.AddComponent(culture);
            culture.SetData("Mittelländische Landbevölkerung");
            possibleCultures.AddComponent(culture);
            culture.SetData("Bornland");
            possibleCultures.AddComponent(culture);
            culture.SetData("Svellttal");
            possibleCultures.AddComponent(culture);
            culture.SetData("Thorwal");
            possibleCultures.AddComponent(culture);
            culture.SetData("Gjalskerland");
            possibleCultures.AddComponent(culture);
            culture.SetData("Nivesenstämme");
            possibleCultures.AddComponent(culture);
            if (!character.AddComponent(possibleCultures))
            {
                return false;
            }

            Skill orientation = new Skill();
            Name name = new Name();
            SkillPoints skillPoints = new SkillPoints();
            name.SetData("Orientierung");
            skillPoints.SetData(1);
            orientation.AddComponent(name);
            orientation.AddComponent(skillPoints);
            if (!character.AddComponent(orientation))
            {
                return false;
            }

            Skill arithmetic = new Skill();
            name.SetData("Rechnen");
            skillPoints.SetData(1);
            arithmetic.AddComponent(name);
            arithmetic.AddComponent(skillPoints);
            if (!character.AddComponent(arithmetic))
            {
                return false;
            }

            return true;
        }

        void CreateTrollzacker(Character character)
        {
            TrollzackerEnum eTrollzacker = default(TrollzackerEnum);
            switch (eTrollzacker)
            {
                case TrollzackerEnum.Default:
                    CreateTrollzackerDefault(character);
                    break;

                case TrollzackerEnum.Rochshaz:
                    CreateTrollzackerRochshaz(character);
                    break;
            }
        }

        bool CreateTrollzackerDefault(Character character)
        {
            Race race = new Race();
            race.SetData("Trollzacker");
            if (!character.AddComponent(race))
            {
                return false;
            }

            GenerationPoints gpCost = new GenerationPoints();
            gpCost.SetData(7);
            if (!character.AddComponent(gpCost))
            {
                return false;
            }
            //HairColour hairColour = new HairColour();
            //EyeColour eyeColour = new EyeColour();
            //Height height = new Height();
            //Weight weight = new Weight();

            CourageModificator MUMod = new CourageModificator();
            MUMod.SetData(2);
            if (!character.AddComponent(MUMod))
            {
                return false;
            }

            WisdomModificator KLMod = new WisdomModificator();
            KLMod.SetData(-1);
            if (!character.AddComponent(KLMod))
            {
                return false;
            }

            ConstitutionModificator KOMod = new ConstitutionModificator();
            KOMod.SetData(1);
            if (!character.AddComponent(KOMod))
            {
                return false;
            }

            StrengthModificator KKMod = new StrengthModificator();
            KKMod.SetData(1);
            if (!character.AddComponent(KKMod))
            {
                return false;
            }

            VitalityModificator LEMod = new VitalityModificator();
            LEMod.SetData(11);
            if (!character.AddComponent(LEMod))
            {
                return false;
            }

            EnduranceModificator AUMod = new EnduranceModificator();
            AUMod.SetData(18);
            if (!character.AddComponent(AUMod))
            {
                return false;
            }

            MagicalResistanceModificator MRMod = new MagicalResistanceModificator();
            MRMod.SetData(-5);
            if (!character.AddComponent(MRMod))
            {
                return false;
            }

            Handicaps handicaps = new Handicaps();
            Handicap handicap = new Handicap();
            handicap.SetData("Platzangst", 6);
            handicaps.AddComponent(handicap);
            if (!character.AddComponent(handicaps))
            {
                return false;
            }

            RecommendedAdvantages recommendedAdvantages = new RecommendedAdvantages();
            Advantage advantage = new Advantage();
            advantage.SetData("Ausdauernd", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Balance", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Eisern", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Entfernungssinn", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Hohe Lebenskraft", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Kampfrausch", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Resistenz gegen Gift", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Resistenz gegen Krankheiten", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Richtungssinn", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Zäher Hund", 0);
            recommendedAdvantages.AddComponent(advantage);
            if (!character.AddComponent(recommendedAdvantages))
            {
                return false;
            }

            RecommendedHandicaps recommendedHandicaps = new RecommendedHandicaps();
            handicap.SetData("Blutrausch", 0);
            recommendedHandicaps.AddComponent(handicap);
            handicap.SetData("Jähzorn", 0);
            recommendedHandicaps.AddComponent(handicap);
            handicap.SetData("Unangenehme Stimme", 0);
            recommendedHandicaps.AddComponent(handicap);
            handicap.SetData("Unansehnlich", 0);
            recommendedHandicaps.AddComponent(handicap);
            if (!character.AddComponent(recommendedHandicaps))
            {
                return false;
            }

            InappropriateAdvantages inappropriateAdvantages = new InappropriateAdvantages();
            advantage.SetData("Gut aussehend", 0);
            inappropriateAdvantages.AddComponent(advantage);
            advantage.SetData("Herausragende Balance", 0);
            inappropriateAdvantages.AddComponent(advantage);
            advantage.SetData("Wohlklang", 0);
            inappropriateAdvantages.AddComponent(advantage);
            if (!character.AddComponent(inappropriateAdvantages))
            {
                return false;
            }

            InappropriateHandicaps inappropriateHandicaps = new InappropriateHandicaps();
            handicap.SetData("Kleinwüchsig", 0);
            inappropriateHandicaps.AddComponent(handicap);
            handicap.SetData("Nahrungsrestriktion", 0);
            inappropriateHandicaps.AddComponent(handicap);
            handicap.SetData("Schlechte Regeneration", 0);
            inappropriateHandicaps.AddComponent(handicap);
            if (!character.AddComponent(inappropriateHandicaps))
            {
                return false;
            }

            CommonCultures commonCultures = new CommonCultures();
            Culture culture = new Culture();
            culture.SetData("Trollzacken");
            commonCultures.AddComponent(culture);
            if (!character.AddComponent(commonCultures))
            {
                return false;
            }

            PossibleCultures possibleCultures = new PossibleCultures();
            culture.SetData("Ferkina");
            possibleCultures.AddComponent(culture);
            culture.SetData("Mittelländische Landbevölkerung");
            possibleCultures.AddComponent(culture);
            culture.SetData("Aranien");
            possibleCultures.AddComponent(culture);
            if (!character.AddComponent(possibleCultures))
            {
                return false;
            }

            Skill athletics = new Skill();
            Name name = new Name();
            SkillPoints skillPoints = new SkillPoints();
            name.SetData("Athletik");
            skillPoints.SetData(1);
            athletics.AddComponent(name);
            athletics.AddComponent(skillPoints);
            if (!character.AddComponent(athletics))
            {
                return false;
            }

            Skill climbing = new Skill();
            name.SetData("Klettern");
            skillPoints.SetData(1);
            climbing.AddComponent(name);
            climbing.AddComponent(skillPoints);
            if (!character.AddComponent(climbing))
            {
                return false;
            }

            Skill selfControl = new Skill();
            name.SetData("Selbstbeherrschung");
            skillPoints.SetData(1);
            selfControl.AddComponent(name);
            selfControl.AddComponent(skillPoints);
            if (!character.AddComponent(selfControl))
            {
                return false;
            }

            Skill sharpSenses = new Skill();
            name.SetData("Sinnesschärfe");
            skillPoints.SetData(1);
            sharpSenses.AddComponent(name);
            sharpSenses.AddComponent(skillPoints);
            if (!character.AddComponent(sharpSenses))
            {
                return false;
            }

            return true;
        }

        bool CreateTrollzackerRochshaz(Character character)
        {
            Race race = new Race();
            race.SetData("Trollzacker");
            if (!character.AddComponent(race))
            {
                return false;
            }

            RaceModification raceMod = new RaceModification();
            raceMod.SetData("Rochshaz");
            if (!character.AddComponent(raceMod))
            {
                return false;
            }

            GenerationPoints gpCost = new GenerationPoints();
            gpCost.SetData(6);
            if (!character.AddComponent(gpCost))
            {
                return false;
            }
            //HairColour hairColour = new HairColour();
            //EyeColour eyeColour = new EyeColour();
            //Height height = new Height();
            //Weight weight = new Weight();

            CourageModificator MUMod = new CourageModificator();
            MUMod.SetData(2);
            if (!character.AddComponent(MUMod))
            {
                return false;
            }

            WisdomModificator KLMod = new WisdomModificator();
            KLMod.SetData(-1);
            if (!character.AddComponent(KLMod))
            {
                return false;
            }

            DexterityModificator FFMod = new DexterityModificator();
            FFMod.SetData(-1);
            if (!character.AddComponent(FFMod))
            {
                return false;
            }

            AgilityModificator GEMod = new AgilityModificator();
            GEMod.SetData(-1);
            if (!character.AddComponent(GEMod))
            {
                return false;
            }
            
            ConstitutionModificator KOMod = new ConstitutionModificator();
            KOMod.SetData(2);
            if (!character.AddComponent(KOMod))
            {
                return false;
            }

            StrengthModificator KKMod = new StrengthModificator();
            KKMod.SetData(2);
            if (!character.AddComponent(KKMod))
            {
                return false;
            }

            VitalityModificator LEMod = new VitalityModificator();
            LEMod.SetData(12);
            if (!character.AddComponent(LEMod))
            {
                return false;
            }

            EnduranceModificator AUMod = new EnduranceModificator();
            AUMod.SetData(20);
            if (!character.AddComponent(AUMod))
            {
                return false;
            }

            MagicalResistanceModificator MRMod = new MagicalResistanceModificator();
            MRMod.SetData(-5);
            if (!character.AddComponent(MRMod))
            {
                return false;
            }

            Handicaps handicaps = new Handicaps();
            Handicap handicap = new Handicap();
            handicap.SetData("Jähzorn", 6);
            handicaps.AddComponent(handicap);
            handicap.SetData("Platzangst", 6);
            handicaps.AddComponent(handicap);
            if (!character.AddComponent(handicaps))
            {
                return false;
            }


            RecommendedAdvantages recommendedAdvantages = new RecommendedAdvantages();
            Advantage advantage = new Advantage();
            advantage.SetData("Ausdauernd", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Balance", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Eisern", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Entfernungssinn", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Hohe Lebenskraft", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Kampfrausch", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Resistenz gegen Gift", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Resistenz gegen Krankheiten", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Richtungssinn", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Zäher Hund", 0);
            recommendedAdvantages.AddComponent(advantage);
            if (!character.AddComponent(recommendedAdvantages))
            {
                return false;
            }

            RecommendedHandicaps recommendedHandicaps = new RecommendedHandicaps();
            handicap.SetData("Blutrausch", 0);
            recommendedHandicaps.AddComponent(handicap);
            handicap.SetData("Jähzorn", 0);
            recommendedHandicaps.AddComponent(handicap);
            handicap.SetData("Randgruppe", 0);
            recommendedHandicaps.AddComponent(handicap);
            handicap.SetData("Unangenehme Stimme", 0);
            recommendedHandicaps.AddComponent(handicap);
            handicap.SetData("Unansehnlich", 0);
            recommendedHandicaps.AddComponent(handicap);
            if (!character.AddComponent(recommendedHandicaps))
            {
                return false;
            }

            InappropriateAdvantages inappropriateAdvantages = new InappropriateAdvantages();
            advantage.SetData("Gut aussehend", 0);
            inappropriateAdvantages.AddComponent(advantage);
            advantage.SetData("Herausragende Balance", 0);
            inappropriateAdvantages.AddComponent(advantage);
            advantage.SetData("Wohlklang", 0);
            inappropriateAdvantages.AddComponent(advantage);
            if (!character.AddComponent(inappropriateAdvantages))
            {
                return false;
            }

            InappropriateHandicaps inappropriateHandicaps = new InappropriateHandicaps();
            handicap.SetData("Kleinwüchsig", 0);
            inappropriateHandicaps.AddComponent(handicap);
            handicap.SetData("Nahrungsrestriktion", 0);
            inappropriateHandicaps.AddComponent(handicap);
            handicap.SetData("Schlechte Regeneration", 0);
            inappropriateHandicaps.AddComponent(handicap);
            if (!character.AddComponent(inappropriateHandicaps))
            {
                return false;
            }

            CommonCultures commonCultures = new CommonCultures();
            Culture culture = new Culture();
            culture.SetData("Trollzacken");
            commonCultures.AddComponent(culture);
            if (!character.AddComponent(commonCultures))
            {
                return false;
            }

            Skill athletics = new Skill();
            Name name = new Name();
            SkillPoints skillPoints = new SkillPoints();
            name.SetData("Athletik");
            skillPoints.SetData(2);
            athletics.AddComponent(name);
            athletics.AddComponent(skillPoints);
            if (!character.AddComponent(athletics))
            {
                return false;
            }

            Skill climbing = new Skill();
            name.SetData("Klettern");
            skillPoints.SetData(1);
            climbing.AddComponent(name);
            climbing.AddComponent(skillPoints);
            if (!character.AddComponent(climbing))
            {
                return false;
            }

            Skill selfControl = new Skill();
            name.SetData("Selbstbeherrschung");
            skillPoints.SetData(1);
            selfControl.AddComponent(name);
            selfControl.AddComponent(skillPoints);
            if (!character.AddComponent(selfControl))
            {
                return false;
            }

            Skill sharpSenses = new Skill();
            name.SetData("Sinnesschärfe");
            skillPoints.SetData(1);
            sharpSenses.AddComponent(name);
            sharpSenses.AddComponent(skillPoints);
            if (!character.AddComponent(sharpSenses))
            {
                return false;
            }

            return true;
        }

        void CreateWaldmensch(Character character)
        {
            WaldmenschEnum eWaldmensch = default(WaldmenschEnum);
            switch (eWaldmensch)
            {
                case WaldmenschEnum.Default:
                    CreateWaldmenschDefault(character);
                    break;
                case WaldmenschEnum.Tocamuyac:
                    CreateWaldmenschTocamuyac(character);
                    break;
            }
        }

        bool CreateWaldmenschDefault(Character character)
        {
            Race race = new Race();
            race.SetData("Waldmensch");
            if (!character.AddComponent(race))
            {
                return false;
            }

            GenerationPoints gpCost = new GenerationPoints();
            gpCost.SetData(5);
            if (!character.AddComponent(gpCost))
            {
                return false;
            }
            //HairColour hairColour = new HairColour();
            //EyeColour eyeColour = new EyeColour();
            //Height height = new Height();
            //Weight weight = new Weight();

            CharismaModificator CHMod = new CharismaModificator();
            CHMod.SetData(1);
            if (!character.AddComponent(CHMod))
            {
                return false;
            }

            AgilityModificator GEMod = new AgilityModificator();
            GEMod.SetData(1);
            if (!character.AddComponent(GEMod))
            {
                return false;
            }

            ConstitutionModificator KOMod = new ConstitutionModificator();
            KOMod.SetData(1);
            if (!character.AddComponent(KOMod))
            {
                return false;
            }

            StrengthModificator KKMod = new StrengthModificator();
            KKMod.SetData(-1);
            if (!character.AddComponent(KKMod))
            {
                return false;
            }

            VitalityModificator LEMod = new VitalityModificator();
            LEMod.SetData(8);
            if (!character.AddComponent(LEMod))
            {
                return false;
            }

            EnduranceModificator AUMod = new EnduranceModificator();
            AUMod.SetData(12);
            if (!character.AddComponent(AUMod))
            {
                return false;
            }

            MagicalResistanceModificator MRMod = new MagicalResistanceModificator();
            MRMod.SetData(-6);
            if (!character.AddComponent(MRMod))
            {
                return false;
            }
                       
            RecommendedAdvantages recommendedAdvantages = new RecommendedAdvantages();
            Advantage advantage = new Advantage();
            advantage.SetData("Ausdauernd", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Balance", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Flink", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Gefahreninstinkt", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Gut aussehend", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Hitzeresistenz", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Resistenz gegen Gift", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Schlangenmensch", 0);
            recommendedAdvantages.AddComponent(advantage);
            if (!character.AddComponent(recommendedAdvantages))
            {
                return false;
            }

            RecommendedHandicaps recommendedHandicaps = new RecommendedHandicaps();
            Handicap handicap = new Handicap();
            handicap.SetData("Kälteempfindlich", 0);
            recommendedHandicaps.AddComponent(handicap);
            if (!character.AddComponent(recommendedHandicaps))
            {
                return false;
            }

            InappropriateAdvantages inappropriateAdvantages = new InappropriateAdvantages();
            advantage.SetData("Hohe Magieresistenz", 0);
            inappropriateAdvantages.AddComponent(advantage);
            advantage.SetData("Kälteresistenz", 0);
            inappropriateAdvantages.AddComponent(advantage);
            advantage.SetData("Schwer zu verzaubern", 0);
            inappropriateAdvantages.AddComponent(advantage);
            if (!character.AddComponent(inappropriateAdvantages))
            {
                return false;
            }

            InappropriateHandicaps inappropriateHandicaps = new InappropriateHandicaps();
            handicap.SetData("Hitzeempfindlich", 0);
            inappropriateHandicaps.AddComponent(handicap);
            handicap.SetData("Nahrungsrestriktion", 0);
            inappropriateHandicaps.AddComponent(handicap);
            if (!character.AddComponent(inappropriateHandicaps))
            {
                return false;
            }

            CommonCultures commonCultures = new CommonCultures();
            Culture culture = new Culture();
            culture.SetData("Dschungelstämme");
            commonCultures.AddComponent(culture);
            culture.SetData("Verlorene Stämme");
            commonCultures.AddComponent(culture);
            culture.SetData("Miniwatu");
            commonCultures.AddComponent(culture);
            culture.SetData("Darna");
            commonCultures.AddComponent(culture);
            culture.SetData("Südaventurien");
            commonCultures.AddComponent(culture);
            culture.SetData("Bukanier");
            commonCultures.AddComponent(culture);
            if (!character.AddComponent(commonCultures))
            {
                return false;
            }

            PossibleCultures possibleCultures = new PossibleCultures();
            culture.SetData("Tocamuyac");
            possibleCultures.AddComponent(culture);
            culture.SetData("Horasreich");
            possibleCultures.AddComponent(culture);
            culture.SetData("Aranien");
            possibleCultures.AddComponent(culture);
            culture.SetData("Tulamidische Stadtstaaten");
            possibleCultures.AddComponent(culture);
            if (!character.AddComponent(possibleCultures))
            {
                return false;
            }
            
            Skill climbing = new Skill();
            Name name = new Name();
            SkillPoints skillPoints = new SkillPoints();
            name.SetData("Klettern");
            skillPoints.SetData(1);
            climbing.AddComponent(name);
            climbing.AddComponent(skillPoints);
            if (!character.AddComponent(climbing))
            {
                return false;
            }

            Skill bodycontrol = new Skill();
            name.SetData("Körperbeherrschung");
            skillPoints.SetData(2);
            bodycontrol.AddComponent(name);
            bodycontrol.AddComponent(skillPoints);
            if (!character.AddComponent(bodycontrol))
            {
                return false;
            }


            Skill sneaking = new Skill();
            name.SetData("Schleichen");
            skillPoints.SetData(2);
            sneaking.AddComponent(name);
            sneaking.AddComponent(skillPoints);
            if (!character.AddComponent(sneaking))
            {
                return false;
            }

            Skill sharpSenses = new Skill();
            name.SetData("Sinnesschärfe");
            skillPoints.SetData(3);
            sharpSenses.AddComponent(name);
            sharpSenses.AddComponent(skillPoints);
            if (!character.AddComponent(sharpSenses))
            {
                return false;
            }

            Skill dancing = new Skill();
            name.SetData("Tanzen");
            skillPoints.SetData(1);
            dancing.AddComponent(name);
            dancing.AddComponent(skillPoints);
            if (!character.AddComponent(dancing))
            {
                return false;
            }

            Skill orientation = new Skill();
            name.SetData("Orientierung");
            skillPoints.SetData(1);
            orientation.AddComponent(name);
            orientation.AddComponent(skillPoints);
            if (!character.AddComponent(orientation))
            {
                return false;
            }

            return true;
        }

        bool CreateWaldmenschTocamuyac(Character character)
        {
            Race race = new Race();
            race.SetData("Waldmensch");
            if (!character.AddComponent(race))
            {
                return false;
            }

            RaceModification raceMod = new RaceModification();
            raceMod.SetData("Tocamuyac");
            if (!character.AddComponent(raceMod))
            {
                return false;
            }

            GenerationPoints gpCost = new GenerationPoints();
            gpCost.SetData(3);
            if (!character.AddComponent(gpCost))
            {
                return false;
            }
            //HairColour hairColour = new HairColour();
            //EyeColour eyeColour = new EyeColour();
            //Height height = new Height();
            //Weight weight = new Weight();

            CharismaModificator CHMod = new CharismaModificator();
            CHMod.SetData(1);
            if (!character.AddComponent(CHMod))
            {
                return false;
            }

            AgilityModificator GEMod = new AgilityModificator();
            GEMod.SetData(1);
            if (!character.AddComponent(GEMod))
            {
                return false;
            }

            ConstitutionModificator KOMod = new ConstitutionModificator();
            KOMod.SetData(1);
            if (!character.AddComponent(KOMod))
            {
                return false;
            }

            StrengthModificator KKMod = new StrengthModificator();
            KKMod.SetData(-1);
            if (!character.AddComponent(KKMod))
            {
                return false;
            }

            VitalityModificator LEMod = new VitalityModificator();
            LEMod.SetData(8);
            if (!character.AddComponent(LEMod))
            {
                return false;
            }

            EnduranceModificator AUMod = new EnduranceModificator();
            AUMod.SetData(12);
            if (!character.AddComponent(AUMod))
            {
                return false;
            }

            MagicalResistanceModificator MRMod = new MagicalResistanceModificator();
            MRMod.SetData(-6);
            if (!character.AddComponent(MRMod))
            {
                return false;
            }

            Handicaps handicaps = new Handicaps();
            Handicap handicap = new Handicap();
            handicap.SetData("Kleinwüchsig", 0);
            handicaps.AddComponent(handicap);
            if (!character.AddComponent(handicaps))
            {
                return false;
            }

            RecommendedAdvantages recommendedAdvantages = new RecommendedAdvantages();
            Advantage advantage = new Advantage();
            advantage.SetData("Ausdauernd", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Balance", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Flink", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Gefahreninstinkt", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Gut aussehend", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Hitzeresistenz", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Resistenz gegen Gift", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Schlangenmensch", 0);
            recommendedAdvantages.AddComponent(advantage);
            if (!character.AddComponent(recommendedAdvantages))
            {
                return false;
            }

            RecommendedHandicaps recommendedHandicaps = new RecommendedHandicaps();
            handicap.SetData("Kälteempfindlich", 0);
            recommendedHandicaps.AddComponent(handicap);
            if (!character.AddComponent(recommendedHandicaps))
            {
                return false;
            }

            InappropriateAdvantages inappropriateAdvantages = new InappropriateAdvantages();
            advantage.SetData("Hohe Magieresistenz", 0);
            inappropriateAdvantages.AddComponent(advantage);
            advantage.SetData("Kälteresistenz", 0);
            inappropriateAdvantages.AddComponent(advantage);
            advantage.SetData("Schwer zu verzaubern", 0);
            inappropriateAdvantages.AddComponent(advantage);
            if (!character.AddComponent(inappropriateAdvantages))
            {
                return false;
            }

            InappropriateHandicaps inappropriateHandicaps = new InappropriateHandicaps();
            handicap.SetData("Hitzeempfindlich", 0);
            inappropriateHandicaps.AddComponent(handicap);
            handicap.SetData("Meeresangst", 0);
            inappropriateHandicaps.AddComponent(handicap);
            handicap.SetData("Nahrungsrestriktion", 0);
            inappropriateHandicaps.AddComponent(handicap);
            if (!character.AddComponent(inappropriateHandicaps))
            {
                return false;
            }

            CommonCultures commonCultures = new CommonCultures();
            Culture culture = new Culture();
            culture.SetData("Tocamuyac");
            commonCultures.AddComponent(culture);
            if (!character.AddComponent(commonCultures))
            {
                return false;
            }

            PossibleCultures possibleCultures = new PossibleCultures();
            culture.SetData("Dschungelstämme");
            possibleCultures.AddComponent(culture);
            culture.SetData("Miniwatu");
            possibleCultures.AddComponent(culture);
            culture.SetData("Waldinsel-Utulus");
            possibleCultures.AddComponent(culture);
            culture.SetData("Südaventurien");
            possibleCultures.AddComponent(culture);
            culture.SetData("Bukanier");
            possibleCultures.AddComponent(culture);
            culture.SetData("Horasreich");
            possibleCultures.AddComponent(culture);
            culture.SetData("Aranien");
            possibleCultures.AddComponent(culture);
            culture.SetData("Tulamidische Stadtstaaten");
            possibleCultures.AddComponent(culture);
            culture.SetData("Mittelländische Städte (Vallusa)");
            possibleCultures.AddComponent(culture);
            if (!character.AddComponent(possibleCultures))
            {
                return false;
            }
            
            Skill bodycontrol = new Skill();
            Name name = new Name();
            SkillPoints skillPoints = new SkillPoints();
            name.SetData("Körperbeherrschung");
            skillPoints.SetData(3);
            bodycontrol.AddComponent(name);
            bodycontrol.AddComponent(skillPoints);
            if (!character.AddComponent(bodycontrol))
            {
                return false;
            }
            
            Skill swimming = new Skill();
            name.SetData("Schleichen");
            skillPoints.SetData(3);
            swimming.AddComponent(name);
            swimming.AddComponent(skillPoints);
            if (!character.AddComponent(swimming))
            {
                return false;
            }

            Skill sharpSenses = new Skill();
            name.SetData("Sinnesschärfe");
            skillPoints.SetData(3);
            sharpSenses.AddComponent(name);
            sharpSenses.AddComponent(skillPoints);
            if (!character.AddComponent(sharpSenses))
            {
                return false;
            }

            Skill orientation = new Skill();
            name.SetData("Orientierung");
            skillPoints.SetData(1);
            orientation.AddComponent(name);
            orientation.AddComponent(skillPoints);
            if (!character.AddComponent(orientation))
            {
                return false;
            }

            return true;
        }

        bool CreateUtulu(Character character)
        {
            Race race = new Race();
            race.SetData("Utulu");
            if (!character.AddComponent(race))
            {
                return false;
            }

            GenerationPoints gpCost = new GenerationPoints();
            gpCost.SetData(9);
            if (!character.AddComponent(gpCost))
            {
                return false;
            }
            //HairColour hairColour = new HairColour();
            //EyeColour eyeColour = new EyeColour();
            //Height height = new Height();
            //Weight weight = new Weight();

            AgilityModificator GEMod = new AgilityModificator();
            GEMod.SetData(1);
            if (!character.AddComponent(GEMod))
            {
                return false;
            }

            ConstitutionModificator KOMod = new ConstitutionModificator();
            KOMod.SetData(1);
            if (!character.AddComponent(KOMod))
            {
                return false;
            }

            VitalityModificator LEMod = new VitalityModificator();
            LEMod.SetData(11);
            if (!character.AddComponent(LEMod))
            {
                return false;
            }

            EnduranceModificator AUMod = new EnduranceModificator();
            AUMod.SetData(12);
            if (!character.AddComponent(AUMod))
            {
                return false;
            }

            MagicalResistanceModificator MRMod = new MagicalResistanceModificator();
            MRMod.SetData(-6);
            if (!character.AddComponent(MRMod))
            {
                return false;
            }

            Advantages advantages = new Advantages();
            Advantage advantage = new Advantage();
            advantage.SetData("Hitzeresistenz", 0);
            advantages.AddComponent(advantage);
            if (!character.AddComponent(advantages))
            {
                return false;
            }

            RecommendedAdvantages recommendedAdvantages = new RecommendedAdvantages();
            advantage.SetData("Ausdauernd", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Balance", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Begabung Musizieren", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Gefahreninstinkt", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Gut aussehend", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Resistenz gegen Gift", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Schlangenmensch", 0);
            recommendedAdvantages.AddComponent(advantage);
            advantage.SetData("Wohlklang", 0);
            recommendedAdvantages.AddComponent(advantage);
            if (!character.AddComponent(recommendedAdvantages))
            {
                return false;
            }

            RecommendedHandicaps recommendedHandicaps = new RecommendedHandicaps();
            Handicap handicap = new Handicap();
            handicap.SetData("Kälteempfindlich", 0);
            recommendedHandicaps.AddComponent(handicap);
            if (!character.AddComponent(recommendedHandicaps))
            {
                return false;
            }

            InappropriateAdvantages inappropriateAdvantages = new InappropriateAdvantages();
            advantage.SetData("Hohe Magieresistenz", 0);
            inappropriateAdvantages.AddComponent(advantage);
            advantage.SetData("Kälteresistenz", 0);
            inappropriateAdvantages.AddComponent(advantage);
            advantage.SetData("Schwer zu verzaubern", 0);
            inappropriateAdvantages.AddComponent(advantage);
            if (!character.AddComponent(inappropriateAdvantages))
            {
                return false;
            }

            InappropriateHandicaps inappropriateHandicaps = new InappropriateHandicaps();
            handicap.SetData("Hitzeempfindlich", 0);
            inappropriateHandicaps.AddComponent(handicap);
            handicap.SetData("Nahrungsrestriktion", 0);
            inappropriateHandicaps.AddComponent(handicap);
            if (!character.AddComponent(inappropriateHandicaps))
            {
                return false;
            }

            CommonCultures commonCultures = new CommonCultures();
            Culture culture = new Culture();
            culture.SetData("Waldinsel-Utulus");
            commonCultures.AddComponent(culture);
            culture.SetData("Verlorene Stämme (Shokubunga)");
            commonCultures.AddComponent(culture);
            culture.SetData("Dschungelstämme (Tschopukikuha)");
            commonCultures.AddComponent(culture);
            culture.SetData("Südaventurien");
            commonCultures.AddComponent(culture);
            culture.SetData("Bukanier");
            commonCultures.AddComponent(culture);
            if (!character.AddComponent(commonCultures))
            {
                return false;
            }

            PossibleCultures possibleCultures = new PossibleCultures();
            culture.SetData("Tocamuyac");
            possibleCultures.AddComponent(culture);
            culture.SetData("Dschungelstämme");
            possibleCultures.AddComponent(culture);
            culture.SetData("Verlorene Stämme");
            possibleCultures.AddComponent(culture);
            culture.SetData("Horasreich");
            possibleCultures.AddComponent(culture);
            culture.SetData("Aranien");
            possibleCultures.AddComponent(culture);
            culture.SetData("Tulamidische Stadtstaaten");
            possibleCultures.AddComponent(culture);
            if (!character.AddComponent(possibleCultures))
            {
                return false;
            }

            Skill climbing = new Skill();
            Name name = new Name();
            SkillPoints skillPoints = new SkillPoints();
            name.SetData("Klettern");
            skillPoints.SetData(1);
            climbing.AddComponent(name);
            climbing.AddComponent(skillPoints);
            if (!character.AddComponent(climbing))
            {
                return false;
            }

            Skill bodycontrol = new Skill();
            name.SetData("Körperbeherrschung");
            skillPoints.SetData(2);
            bodycontrol.AddComponent(name);
            bodycontrol.AddComponent(skillPoints);
            if (!character.AddComponent(bodycontrol))
            {
                return false;
            }


            Skill sneaking = new Skill();
            name.SetData("Schleichen");
            skillPoints.SetData(2);
            sneaking.AddComponent(name);
            sneaking.AddComponent(skillPoints);
            if (!character.AddComponent(sneaking))
            {
                return false;
            }

            Skill sharpSenses = new Skill();
            name.SetData("Sinnesschärfe");
            skillPoints.SetData(3);
            sharpSenses.AddComponent(name);
            sharpSenses.AddComponent(skillPoints);
            if (!character.AddComponent(sharpSenses))
            {
                return false;
            }

            Skill dancing = new Skill();
            name.SetData("Tanzen");
            skillPoints.SetData(1);
            dancing.AddComponent(name);
            dancing.AddComponent(skillPoints);
            if (!character.AddComponent(dancing))
            {
                return false;
            }

            Skill orientation = new Skill();
            name.SetData("Orientierung");
            skillPoints.SetData(1);
            orientation.AddComponent(name);
            orientation.AddComponent(skillPoints);
            if (!character.AddComponent(orientation))
            {
                return false;
            }

            return true;
        }

        bool CreateElf(Character character)
        {
            Race race = new Race();
            race.SetData("Elf");
            if (!character.AddComponent(race))
            {
                return false;
            }


            return true;
        }

        bool CreateHalbelf(Character character)
        {
            Race race = new Race();
            race.SetData("Halbelf");
            if (!character.AddComponent(race))
            {
                return false;
            }


            return true;
        }

        bool CreateZwerg(Character character)
        {
            Race race = new Race();
            race.SetData("Zwerg");
            if (!character.AddComponent(race))
            {
                return false;
            }


            return true;
        }

        bool CreateOrk(Character character)
        {
            Race race = new Race();
            race.SetData("Ork");
            if (!character.AddComponent(race))
            {
                return false;
            }


            return true;
        }

        bool CreateHalbork(Character character)
        {
            Race race = new Race();
            race.SetData("Halbork");
            if (!character.AddComponent(race))
            {
                return false;
            }


            return true;
        }

        bool CreateGoblin(Character character)
        {
            Race race = new Race();
            race.SetData("Goblin");
            if (!character.AddComponent(race))
            {
                return false;
            }


            return true;
        }

        bool CreateAchaz(Character character)
        {
            Race race = new Race();
            race.SetData("Achaz");
            if (!character.AddComponent(race))
            {
                return false;
            }


            return true;
        }
    }
}
