﻿using System.Collections.Concurrent;

namespace Core
{
    public abstract class Component { }

    public class Component<T> : Component
    {
        public bool AddComponent(Component Component)
        {
            return Components.TryAdd(Component.GetType().Name, Component);
        }

        public bool HasComponent<R>()
        {
            return Components.ContainsKey(typeof(R).Name);
        }

        public R GetComponent<R>()
        {
            return (R)(object)Components[typeof(R).Name];
        }

        public T GetData()
        {
            return Data;
        }

        public void SetData(T Data)
        {
            this.Data = Data;
        }

        private T Data;

        private ConcurrentDictionary<string, Component> Components = new ConcurrentDictionary<string, Component>();
    }

    public class Component<T1, T2> : Component
    {
        public void SetData(T1 Key, T2 Value)
        {
            this.Key = Key;
            this.Value = Value;
        }

        private T1 Key;
        private T2 Value;
    }
}