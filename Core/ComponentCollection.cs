﻿using System.Collections.Generic;

namespace Core
{
    public class Character : Component<string> { }

    //Charaktererschaffung
    public class Name : Component<string> { }
    public class GenerationPoints : Component<int> { }
    public class Race : Component<string> { }
    public class RaceModification : Component<string> { }
    public class Culture : Component<string> { }
    public class CultureModification : Component<string> { }
    public class Profession : Component<string> { }
    public class ProfessionModification : Component<string> { }
    public class AdventurePoints : Component<int> { }

    public class CommonCultures : Component<List<Culture>> { }
    public class PossibleCultures : Component<List<Culture>> { }

    //Charaktereckdaten
    public class Sex : Component<char> { }
    public class Age : Component<uint> { } //Berechnung durch Ingame Datum minus BirthDate
    public class BirthDate : Component<string> { }
    public class Height : Component<float> { }
    public class Weight : Component<int> { }
    public class HairColour : Component<string> { }
    public class EyeColour : Component<string> { }
    public class Appearance : Component<string> { }

    public class Portrait : Component<object> { } //image?
    public class CoatOfArms : Component<object> { } //image?

    public class SocialRank : Component<string> { }
    public class Title : Component<string> { }
    public class SocialStanding : Component<uint> { }
    public class Family_Background : Component<string> { }

    //Vor- und Nachteile
    public class Advantage : Component<string, uint> { } //key-value pair
    public class Handicap : Component<string, uint> { } //key-value pair

    public class Advantages : Component<List<Advantage>> { }
    public class Handicaps : Component<List<Handicaps>> { }

    public class RecommendedAdvantages : Component<List<Advantage>> { } //key-value pair
    public class RecommendedHandicaps : Component<List<Handicap>> { }

    public class InappropriateAdvantages : Component<List<Advantage>> { } //key-value pair
    public class InappropriateHandicaps : Component<List<Handicap>> { }

    //Eigenschaften

    //public class Attribute : Component<Component[]> { } //auch hier Name
    //public class AttributeModificator : Component<int> { }
    //public class AttributeStart : Component<uint> { }
    //public class AttributeNow : Component<int> { }

    public class CourageModificator : Component<int> { }
    public class CourageStart : Component<uint> { }
    public class Courage : Component<uint> { }

    public class WisdomModificator : Component<int> { }
    public class WisdomStart : Component<uint> { }
    public class Wisdom : Component<uint> { }

    public class IntuitionModificator : Component<int> { }
    public class IntuitionStart : Component<uint> { }
    public class Intuition : Component<uint> { }

    public class CharismaModificator : Component<int> { }
    public class CharismaStart : Component<uint> { }
    public class Charisma : Component<uint> { }

    public class DexterityModificator : Component<int> { }
    public class DexterityStart : Component<uint> { }
    public class Dexterity : Component<uint> { }

    public class AgilityModificator : Component<int> { }
    public class AgilityStart : Component<uint> { }
    public class Agility : Component<uint> { }

    public class ConstitutionModificator : Component<int> { }
    public class ConstitutionStart : Component<uint> { }
    public class Constitution : Component<uint> { }

    public class StrengthModificator : Component<int> { }
    public class StrengthStart : Component<uint> { }
    public class Strength : Component<uint> { }

    public class SpeedModificator : Component<int> { }
    public class Speed : Component<uint> { }

    //abgeleitete Werte
    public class VitalityModificator : Component<int> { }
    public class Vitality : Component<int> { } //Max-Vitality is calculated
    public class VitalityBought : Component<uint> { } //Max-VitaliyBought is calculated

    public class EnduranceModificator : Component<int> { }
    public class Endurance : Component<int> { }
    public class EnduranceBought : Component<uint> { }

    public class AstralEnergyModificator : Component<int> { }
    //public class AstralEnergyPermanentlyMissing : Component<int> { }
    public class AstralEnergy : Component<int> { }
    public class AstralEnergyBought : Component<uint> { }

    public class KarmalEnergyModificator : Component<int> { }
    public class KarmalEnergy : Component<int> { }

    public class MagicalResistanceModificator : Component<int> { }
    public class MagicalResistance : Component<int> { }
    public class MagicalResistanceBought : Component<uint> { }

    public class BaseInitiativeModificator : Component<int> { }

    public class BaseAttackModificator : Component<int> { }

    public class BaseParryModificator : Component<int> { }

    public class BaseRangedModificator : Component<int> { }

    public class WoundThresholdModificator : Component<int> { }

    //profane Sonderfertigkeiten
    public class SpecialSkill : Component<string> { }

    //Verkürzung der Talente/Fähigkeiten
    public class Skill : Component<Component[]> { } //als erstes der Name
    public class SkillPoints : Component<int> { }
    public class DefaultSkill : Component<bool> { }
    public class SkillCheck : Component<string[]> { }
    public class SkillComplexity : Component<string> { }
    public class FightingSkillAttackValue : Component<uint> { }
    public class FightingSkillParryValue : Component<uint> { }
    public class ArmorHandicap : Component<int> { }
    public class SkillSpecialization : Component<string> { }
    public class LanguageComplexity : Component<uint> { }
    public class Mothertongue : Component<bool> { }

    //Zauber
    public class Spell : Component<Component[]> { } //auch einfach Skill benutzen?
    //public class Spellname : Component<string> { } //siehe Name
    //public class Skillpoints : Component<int> { } //bei Talenten schon erstellt
    //public class SkillCheck : Component<string[]> { }
    public class AgainstMagicalResistence : Component<bool> { }
    public class Technique : Component<string> { }
    public class SpellPreparationTime : Component<int> { }
    public class SpellPreparationTimeUnit : Component<string> { }
    public class Effect : Component<string> { }
    public class Costs : Component<string> { }
    public class Target : Component<string> { }
    public class Reach : Component<string> { }
    public class Spellduration : Component<string> { } //string oder int + string
    public class Maintainable : Component<bool> { }
    public class Modification : Component<string[]> { }
    public class Variants : Component<string[]> { }
    public class ReversalisEffect : Component<string> { }
    public class Countermagic : Component<string> { }
    public class MagicComponents : Component<string[]> { }
    //public class SkillComplexity : Component<string> { } //bei Talenten
    public class Commonness : Component<string> { } //string + int
}